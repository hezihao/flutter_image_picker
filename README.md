# 打包方式

## Android 平台

- flutter build apk

## Web平台

- flutter build web

- flutter build web --web-renderer html

- flutter build web --web-renderer canvaskit

## AndroidStudio 识别不到网易MuMu模拟器

- 手动用adb连接到模拟器，就可以识别到了

```shell
adb connect 127.0.0.1:7555
```