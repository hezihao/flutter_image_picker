import 'package:permission_handler/permission_handler.dart';

/// 权限工具类
class PermissionUtil {
  /// 申请摄像头权限
  static Future<bool> requestCameraPermission() async {
    await [
      Permission.camera,
    ].request();

    if (await Permission.camera.isGranted) {
      return true;
    } else {
      return false;
    }
  }

  /// 申请图库权限
  static Future<bool> requestPhotosPermission() async {
    await [
      Permission.photos,
    ].request();

    if (await Permission.photos.isGranted) {
      return true;
    } else {
      return false;
    }
  }

  /// 申请麦克风权限
  static Future<bool> requestSpeechPermission() async {
    await [
      Permission.speech,
    ].request();

    if (await Permission.speech.isGranted) {
      return true;
    } else {
      return false;
    }
  }

  /// 申请存储权限
  static Future<bool> requestStoragePermission() async {
    await [
      Permission.storage,
    ].request();

    if (await Permission.storage.isGranted) {
      return true;
    } else {
      return false;
    }
  }

  /// 申请定位权限
  static Future<bool> requestLocationPermission() async {
    await [
      Permission.location,
    ].request();

    if (await Permission.location.isGranted) {
      return true;
    } else {
      return false;
    }
  }
}
