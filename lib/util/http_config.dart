import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_image_picker/url.dart';
import 'package:flutter_nb_net/flutter_net.dart';
import 'package:cookie_jar/cookie_jar.dart';

/// Http配置类
class HttpConfig {
  /// 初始化
  static void init() {
    var options = NetOptions.instance;
    // 公共header
    //options.addHeaders({"token": '111'});
    options
         // 基础路径
        .setBaseUrl(Url.getBaseUrl())
        // 超时时间
        .setConnectTimeout(const Duration(milliseconds: 3000))
        // 允许打印log，默认为 true
        .enableLogger(true);
    // cookie
    if(!kIsWeb) {
      options.addInterceptor(CookieManager(CookieJar()));
    }
    options.create();
  }
}
