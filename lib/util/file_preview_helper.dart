import 'package:flutter/material.dart';

import '../image_viewer_page.dart';
import '../video_viewer_page.dart';

/// 文件预览工具类
class FilePreviewHelper {
  /// 是否是图片文件
  static bool isImageFile(String uploadFileUrl) {
    return uploadFileUrl.endsWith("jpg") ||
        uploadFileUrl.endsWith("png") ||
        uploadFileUrl.endsWith("jpeg");
  }

  /// 跳转到图片预览页面
  static void goImageViewerPage(
      BuildContext context, List<String> urls, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ImageViewerPage(
        imageItems: urls,
        defaultIndex: index,
      );
    }));
  }

  /// 跳转到视频预览页面
  static void goVideoViewerPage(BuildContext context, String url) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return VideoViewerPage(
        url: url,
      );
    }));
  }

  /// 跳转到预览页面
  static void goPreviewPage(BuildContext context, String url) {
    if (isImageFile(url)) {
      //图片预览
      goImageViewerPage(context, [url], 0);
    } else {
      //视频预览
      goVideoViewerPage(context, url);
    }
  }
}
