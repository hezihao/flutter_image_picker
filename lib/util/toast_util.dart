import 'package:flutter/material.dart';
import 'package:oktoast/oktoast.dart';

/// Toast 工具类
class ToastUtil {
  static toast(String msg) {
    showToast(
      msg,
      position: ToastPosition.bottom,
      backgroundColor: Colors.black.withOpacity(0.8),
      radius: 13.0,
      textStyle: const TextStyle(fontSize: 18.0, color: Colors.white),
      animationBuilder: const OpacityAnimationBuilder().call,
    );
  }
}
