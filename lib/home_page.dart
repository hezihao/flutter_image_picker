import 'package:flutter/material.dart';
import 'package:flutter_image_picker/upload_history_page.dart';
import 'package:flutter_image_picker/upload_page.dart';

/// 首页
class HomePage extends StatefulWidget {
  const HomePage({super.key, required this.title});

  /// 页面标题
  final String title;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  /// 跳转到上传页面
  void _goUploadPage(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const UploadPage();
    }));
  }

  /// 跳转到上传历史页面
  void _goUploadHistoryPage(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return const UploadHistoryPage();
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.primary,
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              child: const Text("上传"),
              onPressed: () {
                // 跳转到上传页面
                _goUploadPage(context);
              },
            ),
            ElevatedButton(
              child: const Text("上传历史"),
              onPressed: () {
                // 跳转到上传历史页面
                _goUploadHistoryPage(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
