import 'package:fast_cached_network_image/fast_cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';

/// 页面改变回调
typedef PageChangeCallback = void Function(int index);

/// 图片预览页面
class ImageViewerPage extends StatefulWidget {
  /// 图片列表
  final List<String> imageItems;

  /// 默认第几张
  final int defaultIndex;

  /// 切换图片回调
  final PageChangeCallback? pageChanged;

  /// 图片查看方向
  final Axis direction;

  const ImageViewerPage(
      {required this.imageItems,
      this.defaultIndex = 0,
      this.pageChanged,
      this.direction = Axis.horizontal,
      super.key});

  @override
  ImageViewerPageState createState() => ImageViewerPageState();
}

class ImageViewerPageState extends State<ImageViewerPage> {
  /// 当前索引
  int currentIndex = 0;

  @override
  void initState() {
    super.initState();
    currentIndex = widget.defaultIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        color: Colors.black,
        child: Stack(
          children: [
            PhotoViewGallery.builder(
                //边界回弹
                scrollPhysics: const BouncingScrollPhysics(),
                //图片配置
                builder: (BuildContext context, int index) {
                  return PhotoViewGalleryPageOptions(
                    //图片优先走缓存
                    imageProvider:
                        FastCachedImageProvider(widget.imageItems[index]),
                  );
                },
                //横向滚动方向
                scrollDirection: widget.direction,
                //条目数量
                itemCount: widget.imageItems.length,
                //背景装饰
                //backgroundDecoration: const BoxDecoration(color: Colors.black),
                //控制器
                pageController:
                    PageController(initialPage: widget.defaultIndex),
                //页面切换回调
                onPageChanged: (index) => setState(() {
                      currentIndex = index;
                      widget.pageChanged?.call(index);
                    })),
            Positioned(
              bottom: 20,
              child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  child: Text("${currentIndex + 1}/${widget.imageItems.length}",
                      style: const TextStyle(
                        decoration: TextDecoration.none,
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        shadows: [
                          Shadow(color: Colors.black, offset: Offset(1, 1)),
                        ],
                      ))),
            ),
            Positioned(
              //关闭按钮
              top: 40,
              left: 20,
              child: Container(
                alignment: Alignment.centerLeft,
                width: 30,
                child: GestureDetector(
                  onTap: () {
                    //退出预览
                    Navigator.pop(context);
                  },
                  child: const Icon(
                    Icons.close_rounded,
                    size: 30.0,
                    color: Colors.white,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
