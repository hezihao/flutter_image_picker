import 'package:fast_cached_network_image/fast_cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_picker/util/http_config.dart';
import 'package:oktoast/oktoast.dart';

import 'home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //网络请求全局配置
  HttpConfig.init();
  //图片加载全局配置
  await FastCachedImageConfig.init(clearCacheAfter: const Duration(days: 15));
  runApp(const MyApp());
}

/// 主页面
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return OKToast(
      child: MaterialApp(
        title: 'Flutter Image Picker',
        theme: ThemeData(
          //主题色
          primarySwatch: Colors.blue,
          //colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
          //是否使用Material3风格
          useMaterial3: false,
        ),
        home: const HomePage(title: 'Flutter Image Picker'),
      ),
    );
  }
}
