import 'package:flutter/foundation.dart';

class Url {
  /// 基础地址路径
  static String getBaseUrl() {
    //Web运行时，不需要带地址，默认请求部署该网站的服务器
    if (kIsWeb) {
      return "";
    } else {
      return "http://127.0.0.1:8080/";
    }
  }

  /// 文件上传
  static var fileUploadUrl = "/common/upload";

  /// 文件上传历史
  static var uploadHistoryList = "/common/list";

  /// 删除某条文件上传记录
  static var deleteById = "/common/deleteById";
}
