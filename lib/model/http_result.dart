import 'package:flutter_nb_net/flutter_net.dart';

class HttpResult extends BaseNetModel {
  int? code;
  String? msg;

  HttpResult({this.code, this.msg});

  HttpResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['msg'] = msg;
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return HttpResult.fromJson(json);
  }
}
