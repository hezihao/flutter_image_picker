class FileUploadResult {
  int? code;
  String? msg;
  String? data;

  FileUploadResult({this.code, this.msg, this.data});

  FileUploadResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msg = json['msg'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['msg'] = msg;
    data['data'] = this.data;
    return data;
  }
}
