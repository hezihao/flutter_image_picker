import 'package:flutter_nb_net/flutter_net.dart';

class UploadHistoryResult extends BaseNetModel {
  int? code;
  String? msg;
  List<Data>? data;

  UploadHistoryResult({this.code, this.msg, this.data});

  UploadHistoryResult.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    msg = json['msg'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['code'] = code;
    data['msg'] = msg;
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  fromJson(Map<String, dynamic> json) {
    return UploadHistoryResult.fromJson(json);
  }
}

class Data {
  String? id;
  String? fileName;
  String? filePath;
  String? url;
  String? createTime;

  Data({this.id, this.fileName, this.filePath, this.url, this.createTime});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fileName = json['fileName'];
    filePath = json['filePath'];
    url = json['url'];
    createTime = json['createTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['fileName'] = fileName;
    data['filePath'] = filePath;
    data['url'] = url;
    data['createTime'] = createTime;
    return data;
  }
}
